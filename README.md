# coloc

coloc script v 0.1
works with Python 2.7

Usage:

python do_coloc.py -i file.txt -p1 0.00001 -p2 0.00001 -p12 0.0001

p1, p2 and p12 are optional. If not given, p1 = p2 = 1/number_of_variants and p12 = p1*10

Add later: parameter to specify weights in Wakefield bf. Right now, take mean BF from weights of 0.01, 0.1 and 0.5

Input file contains five columns:

rsid beta1 se1 beta2 se2


