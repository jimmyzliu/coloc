'''
coloc script v 0.1
works with Python 2.7

Usage:
python do_coloc.py -i file.txt -p1 0.00001 -p2 0.00001 -p12 0.0001
p1, p2 and p12 are optional. If not given, p1 = p2 = 1/number_of_variants and p12 = p1*10
Add later: parameter to specify weights in Wakefield bf. Right now, take mean BF from weights of 0.01, 0.1 and 0.5

input file contains five columns:
rsid beta1 se1 beta2 se2
'''

from scipy import stats
import math
import sys

def is_number(s):
    try:
        float(s)
        return True
    except ValueError:
        return False

def get_wbf(beta,se,w=0.1):
    z = beta/se
    if z == 0:
        return 1
    v = se**2
    r = w/(v+w)
    wbf = math.sqrt(1-r) * math.exp(z**2/2*r)
    return wbf

def do_coloc(wbf1,wbf2,p1,p2,p12):
    wbf1 = [i for i in wbf1 if not math.isnan(i)]
    wbf2 = [i for i in wbf2 if not math.isnan(i)]
    p_h1 = p1 * sum(wbf1)
    p_h2 = p2 * sum(wbf2)
    p_h3 = p1 * p2 * sum(wbf1) * sum(wbf2) - ((p1 * p2) / p12 * p_h4)
    p_h4 = p12 * sum([wbf1[i]*wbf2[i] for i in range(len(wbf1))])
    pp1 = p_h1/(1 + p_h1 + p_h2 + p_h3 + p_h4)
    pp2 = p_h2/(1 + p_h1 + p_h2 + p_h3 + p_h4)
    pp3 = p_h3/(1 + p_h1 + p_h2 + p_h3 + p_h4)
    pp4 = p_h4/(1 + p_h1 + p_h2 + p_h3 + p_h4)
    return [pp1,pp2,pp3,pp4]


args = sys.argv
for i in range(len(args)):
    if args[i] == "-i":
        input_file = args[i+1]
    if args[i] == "-p1":
        p1 = float(args[i+1])
    if args[i] == "-p2":
        p2 = float(args[i+1])
    if args[i] == "-p12":
        p12 = float(args[i+1])

merge_dict = {i.split()[0]:i.split()[1:] for i in open(input_file,'r')}

wbf1 = []
wbf2 = []
for i in merge_dict:
    beta1 = float(merge_dict[i][0])
    se1 = float(merge_dict[i][1])
    beta2 = float(merge_dict[i][2])
    se2 = float(merge_dict[i][3])
    wbf1.append(sum([get_wbf(beta1,se1,w=0.01),get_wbf(beta1,se1,w=0.1),get_wbf(beta1,se1,w=0.5)])/3)
    wbf2.append(sum([get_wbf(beta2,se2,w=0.01),get_wbf(beta2,se2,w=0.1),get_wbf(beta2,se2,w=0.5)])/3)

if not "p1" in globals():
    p1 = 1.0/len(wbf1)
if not "p2" in globals():
    p2 = 1.0/len(wbf2)
if not "p12" in globals():
    p12 = p1/10

coloc = do_coloc(wbf1,wbf2,p1,p2,p12)

print "pp1 pp2 pp3 pp4"
print " ".join([str(i) for i in coloc])
